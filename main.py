import discord
import os
import random
import sqlite3
from discord.ext import commands
from dotenv import load_dotenv
from userinfo import wakeup, bonkgifs, usermap, emojimap, bonkadmin, honkadmin
from helpdocs import helpdocs, version, releasenotes, gitrepo

load_dotenv()
TOKEN = os.getenv('TOKEN')

intents = discord.Intents.all()
bot = commands.Bot(command_prefix='$',intents=intents)

@bot.event
async def on_ready():
    #guild = bot.get_guild()
    #channel = guild.get_channel()
    #await channel.send(wakeup)
    print('We have logged in as {0.user}'.format(bot))
    
@bot.event
async def on_message(message):
    await bot.process_commands(message)

    if message.author == bot.user:
        return

    msg = message.content.upper()

    if "ELDEN RING" in msg:
        await message.channel.send('Ohhhh, Elden Ring!')

    for key, emoji in emojimap.items():
        if key in msg:
            for botemoji in bot.emojis:
                if emoji in str(botemoji):
                    await message.add_reaction(botemoji)

    if "SPEEDRUNNING" in msg or "RADICAL LEFT" in msg:
        await message.channel.send("You're a beta male, Sonic")
        
    if message.author.id == 204400851194740736:
        if "MARIO" in msg:
            bonkcount = bonksequence(message.author.id)
            await message.channel.send(f"{message.author.mention}, you have been bonked!\n\nYou have been bonked {bonkcount} times.")
            await message.channel.send(getgif("mario"))
            
    for user, words in usermap.items():
        if user == message.author.id:
            for keyword in words.keys():
                if keyword in msg:
                    bonkcount = bonksequence(message.author.id)
                    await message.channel.send(f"{message.author.mention}, you have been bonked!\n\nYou have been bonked {bonkcount} times.")
                    await message.channel.send(bonkgifs[usermap[user][keyword]])


@bot.command(help=helpdocs["git"])
async def git(ctx):
    msg = "dont judge my code thx :call_me: -Alanna\n\n"
    msg += gitrepo
    await ctx.send(msg)


@bot.command(help=helpdocs["release"])
async def release(ctx, versionarg=None):
    if versionarg and versionarg in releasenotes and versionarg != version:
        await ctx.send(f"Release Notes for {versionarg}")
        await ctx.send(releasenotes[versionarg]) 
    else:
        await ctx.send(f"Current Release Notes for {version}")
        await ctx.send(releasenotes[version])
    

# currently bugged
@bot.command(help=helpdocs["eldenring"])
async def eldenring(ctx, member: discord.Member=None):

    voice = discord.utils.get(ctx.bot.voice_clients, guild=ctx.guild)

    if not voice:
        if not member:
            voice_status = ctx.author.voice
            if not voice_status:
                return await ctx.send('https://youtu.be/jFbZ15dpwC4')

            channel = ctx.author.voice.channel
        else:
            voice_status = member.voice
            if not voice_status:
                return await ctx.send('https://youtu.be/jFbZ15dpwC4')

            channel = member.voice.channel

        voice = await channel.connect()

    voice.play(discord.FFmpegPCMAudio("music/Ohhh_Elden_Ring.mp3"))

# currently bugged
@bot.command(help=helpdocs["impostor"])
async def impostor(ctx, member: discord.Member=None):
    print("here")
    voice = discord.utils.get(ctx.bot.voice_clients, guild=ctx.guild)

    if not voice and member:
        voice_status = member.voice
        if voice_status:
            channel = member.voice.channel
            
            voice = await channel.connect()
            voice.play(discord.FFmpegPCMAudio("music/amongusfortnite.mp3"))
            #await voice_client.disconnect()

@bot.command(help=helpdocs["goodnight"])
async def goodnight(ctx):
    voice_client = discord.utils.get(ctx.bot.voice_clients, guild=ctx.guild)

    if voice_client == None:
        return await ctx.send("bro i am literally just s\ni am just standing here")
    
    await voice_client.disconnect()

@bot.command(help=helpdocs["gtfo"])
async def gtfo(ctx):
    await ctx.send("That's no way to talk to your grandpa!\n(try $goodnight)")
    

@bot.command(help=helpdocs["duck"])
async def duck(ctx, member: discord.Member=None):
    ducksequence(ctx.author.id)

    if member:
        mention = member.mention
        await ctx.send(content=f"get ducked {mention}", file=discord.File('img/duck.jpg'))
    else:
        await ctx.send(content=f"get ducked", file=discord.File('img/duck.jpg'))

# shh
#@bot.command()
#async def duckcount(ctx, member: discord.Member=None):
#    if member:
#        userid = member.id
#        mention = member.mention
#    else:
#        userid = ctx.author.id
#        mention = ctx.author.mention

#    connection = sqlite3.connect('bonk.db')
#    cursor = connection.cursor()

#    cursor.execute("select user, friendpoints from bonks where user=:id", {"id": userid})
#    data=cursor.fetchone()
#    if data is None or data[1] is None or data[1] == 0:
#        await ctx.send(f"{mention}, you do not have any friend points.")
#    else:
#        await ctx.send(f"{mention}, you have ducked a friend {data[1]} times! You're such a good friend:)")


@bot.command(help=helpdocs["bonk"])
async def bonk(ctx, member: discord.Member=None):
    if member:
        userid = member.id
        mention = member.mention
    else:
        userid = ctx.author.id
        mention = ctx.author.mention
    print("user %i has been bonked" % userid)

    bonkcount = bonksequence(userid)

    await ctx.send(f"{mention}, you have been bonked!\n\nYou have been bonked {bonkcount} times.")
    await ctx.send(getgif())

@bot.command(help=helpdocs["bonkcount"])
async def bonkcount(ctx, member: discord.Member=None):
    if member:
        userid = member.id
        mention = member.mention
    else:
        userid = ctx.author.id
        mention = ctx.author.mention

    connection = sqlite3.connect('bonk.db')
    cursor = connection.cursor()

    cursor.execute("select user, bonks from bonks where user=:id", {"id": userid})
    data=cursor.fetchone()
    if data is None:
        await ctx.send(f"{mention}, you have not been bonked.")
    else:
        await ctx.send(f"{mention}, you have been bonked {data[1]} times!")


@bot.command(help=helpdocs["unbonk"])
async def unbonk(ctx, member: discord.Member=None):
    if ctx.author.id not in bonkadmin:
        if not member or ctx.author.id == member.id:
            return await ctx.send(f"{ctx.author.mention} - you *would* try to unbonk yourself, wouldn't you?")
        else:
            return await ctx.send(f"{ctx.author.mention} touch grass")

    if member:
        userid = member.id
        mention = member.mention
    else:
        userid = ctx.author.id
        mention = ctx.author.mention

    connection = sqlite3.connect('bonk.db')
    cursor = connection.cursor()

    cursor.execute("select * from bonks where user=:id", {"id": userid})
    bonkcount = cursor.fetchone()[1]-1
    cursor.execute("update bonks set bonks=:bc where user=:id", {"bc": bonkcount, "id": userid})
    connection.commit()
    connection.close()
    await ctx.send(f"{mention} you have been blessed with an unbonk on this day")

    
@bot.command(help=helpdocs["honk"])
async def honk(ctx, member: discord.Member=None):
    if ctx.author.id not in honkadmin:
        return await ctx.send(f"{ctx.author.mention}, boogie on down somewhere else")

    if member:
        userid = member.id
        mention = member.mention
    else:
        userid = ctx.author.id
        mention = ctx.author.mention
    print("user %i has been honked" % userid)

    honkcount = honksequence(userid)

    await ctx.send(f"{mention}, you have been honked!\n\nYou have been honked {honkcount} times.")
    await ctx.send(getgif("honks"))
    
    
@bot.command(help=helpdocs["honkcount"])
async def honkcount(ctx, member: discord.Member=None):
    if member:
        userid = member.id
        mention = member.mention
    else:
        userid = ctx.author.id
        mention = ctx.author.mention

    connection = sqlite3.connect('bonk.db')
    cursor = connection.cursor()

    cursor.execute("select user, honks from bonks where user=:id", {"id": userid})
    data=cursor.fetchone()
    if data is None or data[1] is None:
        await ctx.send(f"{mention}, you have not been honked.")
    else:
        await ctx.send(f"{mention}, you have been honked {data[1]} times!")

    
    
@bot.command(help=helpdocs["leaderboard"])
async def leaderboard(ctx):
    connection = sqlite3.connect('bonk.db')
    cursor = connection.cursor()

    cursor.execute("select * from bonks order by bonks desc limit 5")
    data=cursor.fetchall()
    connection.close()

    if data is None:
        return await ctx.send("I have no bonk records for you.")

    msglist = []
    msglist.append("Bonks\t\|\tUser\t\t\n----------------------------\n")

    for row in data:
        user = await bot.fetch_user(row[0])
        msglist.append(f"{row[1]}  \t\t\t|\t{user.mention}\n")
        
    embed = discord.Embed(title="Bonk Leaderboards :trophy:", description=''.join(msglist))
    await ctx.send(embed=embed)

    
@bot.command(help=helpdocs["roll"])
async def roll(ctx, dice="20"):
    errmsg = "learn how to use dice idk how to help you (hint: $roll -h)"
    hintmsg = """
dirty $roll help
- $roll [no arguments]: defaults to d20
- $roll ydx: roll dx dice y times
- $roll x: roll dx
- $roll dx: roll dx
"""
        
    if dice.isnumeric():
        if int(dice) > 500:
            return await ctx.send("are you trying to kill me")
        await ctx.send(f"Rolling a d{dice}:")
        await ctx.send(f"You rolled a {random.randrange(1,int(dice)+1)}!")
    elif dice[0].upper() == 'D' and dice[1:].isnumeric():
        if int(dice[1:]) > 500:
            return await ctx.send("are you trying to kill me")
        await ctx.send(f"Rolling a d{dice[1:]}:")
        await ctx.send(f"You rolled a {random.randrange(1,int(dice[1:])+1)}!")
    elif dice[0].isnumeric():
        i = 0
        while dice[i].isnumeric():
            i += 1
        if dice[i].upper() != 'D' or not dice[i+1:].isnumeric():
            return await ctx.send(errmsg)
        if int(dice[:i]) > 500:
            return await ctx.send("pls dont")
        elif int(dice[i+1:]) > 500:
            return await ctx.send("are you trying to kill me")
        result = "You rolled:\n"
        for _ in range(0, int(dice[:i])):
            result += str(random.randrange(1,int(dice[i+1:])+1)) + " "
        await ctx.send(f"Rolling {dice[:i]} d{dice[i+1:]}:")
        await ctx.send(result)
    elif dice == "clip":
        await ctx.send('https://www.youtube.com/watch?v=A5U8ypHq3BU')
    elif dice == "-h" or dice == "-help" or dice == "-H":
        await ctx.send(hintmsg)
    else:
        await ctx.send(errmsg)
    
    
def honksequence(userid):
    connection = sqlite3.connect('bonk.db')
    cursor = connection.cursor()

    cursor.execute("select user, honks from bonks where user=:id", {"id": userid})
    data=cursor.fetchone()
    if data is None:
        cursor.execute("insert into bonks (user, bonks, honks, friendpoints) values (:id, 0, 1, 0)", {"id": userid})
        connection.commit()
        honkcount = 1
    elif data[1] is None:
        cursor.execute("update bonks set honks=1 where user=:id", {"id": userid})
        connection.commit()
        honkcount = 1
    else:
        honkcount = data[1]+1
        cursor.execute("update bonks set honks=:hc where user=:id", {"hc": honkcount, "id": userid})
        connection.commit()

    connection.close()
    return honkcount


def ducksequence(userid):
    connection = sqlite3.connect('bonk.db')
    cursor = connection.cursor()

    cursor.execute("select user, friendpoints from bonks where user=:id", {"id": userid})
    data=cursor.fetchone()
    if data is None:
        cursor.execute("insert into bonks (user, bonks, honks, friendpoints) values (:id, 0, 0, 1)", {"id": userid})
        connection.commit()
        fp = 1
    elif data[1] is None:
        cursor.execute("update bonks set friendpoints=1 where user=:id", {"id": userid})
        connection.commit()
        fp = 1
    else:
        fp = data[1]+1
        cursor.execute("update bonks set friendpoints=:fp where user=:id", {"fp": fp, "id": userid})
        connection.commit()

    connection.close()
    return fp


def bonksequence(userid):
    connection = sqlite3.connect('bonk.db')
    cursor = connection.cursor()

    cursor.execute("select * from bonks where user=:id", {"id": userid})
    data=cursor.fetchone()
    if data is None:
        cursor.execute("insert into bonks (user, bonks, honks, friendpoints) values (:id, 1, 0, 0)", {"id": userid})
        connection.commit()
        bonkcount = 1
    elif data[1] is None:
        cursor.execute("update bonks set bonks=1 where user=:id", {"id": userid})
        connection.commit()
        bonkcount = 1
    else:
        bonkcount = data[1]+1
        cursor.execute("update bonks set bonks=:bc where user=:id", {"bc": bonkcount, "id": userid})
        connection.commit()

    connection.close()
    return bonkcount

def getgif(category="general"):
    return bonkgifs[category][random.randrange(0,len(bonkgifs[category]))]


bot.run(TOKEN)
helpdocs = {
    "eldenring" : "post a link to the famous quote or have grandpa jump into chat with you (bugged)",
    "impostor" : "bugged; don't ask",
    "goodnight" : "dismiss grandpa from voice (politely)",
    "gtfo" : "don't be rude!",
    "bonk" : "punish the sinners",
    "bonkcount" : "witness the extent of one's sins",
    "unbonk" : "cleanse a heathen of their sins",
    "honk" : "honk",
    "honkcount" : "*smooth jazz music*",
    "leaderboard" : "see how far behind graves you are",
    "git" : "gets grandpa's git repo for the curious",
    "duck" : "giv self love duck",
    "release" : "summarizes new changes/commands from the latest release",
    "roll" : "dirty dice roller"
}

gitrepo = "https://gitlab.com/amceache/elden-grandpa"

version = "1.4.1"

releasenotes = {
    "1.4.0" : """
**New commands:**
- git
- release
- duck
- roll
**Other:**
- actually set up help docs
- consolidate bonk gif getters
- move atlas bonk back to `userinfo.py`
- fix bug where bonks would not update if someone were honked before they were ever bonked
- looked at pricing for raspberry pis for grandpa's future home
""",
    "1.4.1" : """
**Other:**
- fix $roll bug
- add $roll clip support for alice
- update how release notes work; users can now call previous versions of release notes (if they exist)
"""
}